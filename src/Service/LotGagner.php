<?php

namespace App\Service;


class LotGagner
{
    # Definition de l'espace pour la génération
    const RAND_LOT = 5;
    public function getLotGaner(): string
    {
            
            $lot = "";
            # Création du nombre aléatoire dans cet espace
            $rand = mt_rand(1, self::RAND_LOT);
            //echo ($rand);
            # Definition des valeurs et leur probabilité
            $lots = array( "Une Tesla" => 0.01, "Un weekend à la montagne" => 0.9, "Une PS5" => 0.1, "Un PC Gamer" =>0.3, "Un jeu de cartes" =>0.5);

            # On tri par probabilité croissante
            asort($lots);

            $i=0;
            // Pour chaque élément si le nombre est compris entre la valeur précedente et la valeur actuelle, alors on prend cet élément
            foreach ($lots as $name => $value ) {
            if ($rand <= $i+=($value*self::RAND_LOT)) {
                $lot = $name;
                break;
            }
            
            }
            "use strict";

        return $lot;
    }
}