### Tirage-au-sort


#### How to run the project 



```bash 

$- git clone https://gitlab.com/omrani.rihab/tirage-au-sort.git

$- cd tirage-au-sort/

#Creating the Database

php bin/console doctrine:database:create

#Migrations: Creating the Database Tables/Schema

php bin/console make:migration

php bin/console doctrine:migrations:migrate  

#Loading Fixtures
php bin/console doctrine:fixtures:load

#Run project

$- symfony serve -d 
```


