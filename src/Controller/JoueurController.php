<?php

namespace App\Controller;

use App\Entity\Joueur;
use App\Form\JoueurType;
use App\Repository\JoueurRepository;
use App\Repository\LotRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\LotGagner;

#[Route('/joueur')]
class JoueurController extends AbstractController
{

    #[Route('/new', name: 'app_joueur_new', methods: ['GET', 'POST'])]
    public function new(Request $request, JoueurRepository $joueurRepository, LotGagner $lotGagner, LotRepository $lotRepository): Response
    {
        $joueur = new Joueur();
        $form = $this->createForm(JoueurType::class, $joueur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            $getLot = $lotRepository->findOneByName($lotGagner->getLotGaner());
            
            $joueur->setLot($getLot);
            $joueurRepository->save($joueur, true);

            return $this->render('joueur/show.html.twig', [
                'joueur' => $joueur,
                'lot' => $lotGagner->getLotGaner()
            ]);
        } 

       return $this->renderForm('joueur/new.html.twig', [
            'joueur' => $joueur,
            'form' => $form,
        ]);
    }

}
