<?php

namespace App\DataFixtures;

use App\Entity\Lot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;




class LotFixture extends Fixture

{

    public function load(ObjectManager $manager): void
    {
        

        $lots = array( "Une Tesla" => 0.01, "Un weekend à la montagne" => 0.9, "Une PS5" => 0.1, "Un PC Gamer" =>0.3, "Un jeu de cartes" =>0.5);

        foreach ($lots as $name => $value ) {
            $lot = new Lot();
            $lot->setName($name);

            $manager->persist($lot);
         
        }

        $manager->flush();
        
    }
}
